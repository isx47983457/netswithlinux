Cisco 1700 series R17x05 Eric Escriba 17/01/2017  

C1700 platform with 32768 Kbytes of main memory

program load complete, entry point: 0x80008000, size: 0x542624
Self decompressing the image : ################################################################################################### [OK]


Smart Init is disabled. IOMEM set to: 25



PMem allocated: 25165824 bytes; IOMem allocated: 8388608 bytes

              Restricted Rights Legend

Use, duplication, or disclosure by the Government is
subject to restrictions as set forth in subparagraph
(c) of the Commercial Computer Software - Restricted
Rights clause at FAR sec. 52.227-19 and subparagraph
(c) (1) (ii) of the Rights in Technical Data and Computer
Software clause at DFARS sec. 252.227-7013.

           cisco Systems, Inc.
           170 West Tasman Drive
           San Jose, California 95134-1706



Cisco Internetwork Operating System Software 
IOS (tm) C1700 Software (C1700-Y-M), Version 12.3(1a), RELEASE SOFTWARE (fc1)
Copyright (c) 1986-2003 by cisco Systems, Inc.
Compiled Fri 06-Jun-03 20:01 by dchih
Image text-base: 0x80008120, data-base: 0x809DA464

cisco 1720 (MPC860T) processor (revision 0x601) with 24576K/8192K bytes of memory.
Processor board ID JAD05390DRD (3330783564), with hardware revision 0000
MPC860T processor: part number 0, mask 32
Bridging software.
X.25 software, Version 3.0.0.
1 FastEthernet/IEEE 802.3 interface(s)
2 Low-speed serial(sync/async) network interface(s)
32K bytes of non-volatile configuration memory.
8192K bytes of processor board System flash (Read/Write)

