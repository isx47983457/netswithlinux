EJERCICIO MIKROTIK 2.

OBJETIVO: CREAR DOS REDES WIFIS CON DISTINTOS TIPOS DE SEGURIDAD


 MIKROTIK
 +-------------------+
 |  1  2  3  4  wifi |
 +-------------------+
    |  |  |  |
    |  |  |  |
    |  |  |  +
    |  |  |  Conexión a un switch  con vlans
    |  |  +
    |  |  Interface de red placa base
    |  +
    |  Interface USB
    |  Ip 192.168.88.1XX/24
    +
    Punto de red del aula
    para salida a internet

## Pasos para conseguir tener 2 redes wifis separadas con distintos
niveles de seguridad.

Recordamos que:
- Solo disponemos de una interface wifi que trabaja en la banda de 2,4GHz.
- Este modelo de mikrotik dispone de 4 interfaces ethernet y 1 interface wifi
- En el PC dispondremos de una interface USB que es la que usaremos para
configurar el router por el puerto 2

Para conseguir dos redes wifi a partir de una sola interface hay que crear
sub-interfaces o interfaces virtuales. Cada interface virtual ha de estar 
asociada a un id de vlan (número de 1 a 2048)

Si queremos comunicarnos en la misma red entre un dispositivo wifi y un dispositivo
cableado hay que crear bridges entre la interface wifi y la interface ethernet.

### Eliminar configuración inicial
La configuración inicial que viene con la mikrotik podemos consultarla haciendo 
/export y obtenmos:

```
/interface bridge
add admin-mac=6C:3B:6B:30:B8:4F auto-mac=no comment=defconf name=bridge
/interface wireless
set [ find default-name=wlan1 ] band=2ghz-b/g/n channel-width=20/40mhz-Ce \
    disabled=no distance=indoors frequency=auto mode=ap-bridge ssid=\
    MikroTik-30B852 wireless-protocol=802.11
/interface ethernet
set [ find default-name=ether2 ] name=ether2-master
set [ find default-name=ether3 ] master-port=ether2-master
set [ find default-name=ether4 ] master-port=ether2-master
/ip neighbor discovery
set ether1 discover=no
set bridge comment=defconf
/ip pool
add name=default-dhcp ranges=192.168.88.10-192.168.88.254
/ip dhcp-server
add address-pool=default-dhcp disabled=no interface=bridge name=defconf
/interface bridge port
add bridge=bridge comment=defconf interface=ether2-master
add bridge=bridge comment=defconf interface=wlan1
/ip address
add address=192.168.88.1/24 comment=defconf interface=bridge network=\
    192.168.88.0
/ip dhcp-client
add comment=defconf dhcp-options=hostname,clientid disabled=no interface=ether1
/ip dhcp-server network
add address=192.168.88.0/24 comment=defconf gateway=192.168.88.1
/ip dns
set allow-remote-requests=yes
/ip dns static
add address=192.168.88.1 name=router
/ip firewall filter
add chain=input comment="defconf: accept ICMP" protocol=icmp
add chain=input comment="defconf: accept established,related" connection-state=\
    established,related
add action=drop chain=input comment="defconf: drop all from WAN" in-interface=\
    ether1
add action=fasttrack-connection chain=forward comment="defconf: fasttrack" \
    connection-state=established,related
add chain=forward comment="defconf: accept established,related" \
    connection-state=established,related
add action=drop chain=forward comment="defconf: drop invalid" connection-state=\
    invalid
add action=drop chain=forward comment="defconf:  drop all from WAN not DSTNATed" \
    connection-nat-state=!dstnat connection-state=new in-interface=ether1
/system routerboard settings
set boot-device=flash-boot cpu-frequency=650MHz protected-routerboot=disabled
/tool mac-server
set [ find default=yes ] disabled=yes
add interface=bridge
/tool mac-server mac-winbox
set [ find default=yes ] disabled=yes

```

A continuación modificamos todas esas partes de la configuración que no nos
interesan para dejar la mikrotik pelada, sin configuraciones iniciales
que interfieran con nuestra nueva programación del router:

```
# Cambiamos dirección IP para que la gestione directamente la interface 2
ip address set 0 interface=ether2-master

# Eliminamos el bridge

/interface bridge port remove 1
/interface bridge port remove 0
/interface bridge remove 0  

# Eliminamos que un puerto sea master de otro

/interface ethernet set [ find default-name=ether3 ] master-port=none
/interface ethernet set [ find default-name=ether4 ] master-port=none

# Cambiamos nombres a los puertos

/interface ethernet set [ find default-name=ether1 ] name=eth1
/interface ethernet set [ find default-name=ether2 ] name=eth2
/interface ethernet set [ find default-name=ether3 ] name=eth3
/interface ethernet set [ find default-name=ether4 ] name=eth4

# Deshabilitamos la wifi

/interface wireless set [ find default-name=wlan1 ] disabled=yes

# Eliminamos servidor y cliente dhcp

/ip pool remove 0
/ip dhcp-server network remove 0
/ip dhcp-server remove 0
/ip dhcp-client remove 0
/ip dns static remove 0
/ip dns set allow-remote-requests=no

# Eliminamos regla de nat masquerade
/ip firewall nat remove 0

```

Ahora le cambiamos el nombre y guardamos la configuración inicial por
si la necesitamos restaurar en cualquier momento
```
/system identity set name=mkt01eric
/system backup save name="20170317_zeroconf"
```

###[ejercicio1] Explica el procedimiento para resetear el router y restaurar
este backup, verifica con /export que no queda ninguna configuración inicial 
que pueda molestarnos para empezar a programar el router


    Escribimos en la consola del mikrotic lo siguiente para resetear la configuracion
    del router:
    
	/system reset-configuration
	
	
	Para restaurar cualquier backup ejecutaremos la siguiente orden:
	
	/system backup load name"nombre_backup"
	
	
	Aún así con la orden /file print podemos ver todos los ficheros guardados donde 
	se encuentran los backups.
	

### Crear interfaces y bridges

Primero hay que pensar en capa 2, vamos a crear un bridge por cada red wifi
que tenga un punto de red cableado asociado a una vlan

Trabajaremos con dos vlans:

*1XX: red pública
*2XX: red privada

Hay que crear unas interfaces virtuales wifi adicional y cambiar los ssids
para reconocerlas cuando escaneemos las wifis

```
/interface wireless set 0 name=wFree ssid="free123"
/interface wireless add ssid="private223 name=wPrivate" master-interface=wFree
```



###[ejercicio2] Explica por qué aparecen las interfaces wifi como disabled
al hacer /interface print. Habilita y deshabilita estar interfaces. Cambiales
el nombre a wFree wPrivate y dejalas deshabilitadas

	Para habilitar y deshabilitar las interfaces utilizaremos los siguientes comandos:
	
	/interface wireless enable 
	/interface wireless disable 
	
	Para cambiarles el nombre utilizaremos el set:
	
	[admin@mkt01eric] > /interface wireless set wlan1 name=wFree 
	[admin@mkt01eric] > /interface wireless set wlan2 name=wPrivate
	[admin@mkt01eric] > /interface print (para comprovar)
	
	Para ver si la interficie esta deshabilitada lo veremos si si al lado del nombre 
	tiene una X	. Ejemplo:
	
	 4  X  wFree                               wlan             1500  1600       2290 6C:3B:6B:C2:27:97
	 5  X  wPrivate                            wlan                   1600       2290 6E:3B:6B:C2:27:97

	
## Crear interfaces virtuales y hacer bridges

'''
/interface vlan add name eth4-vlan1XX vlan-id=1XX interface=eth4
/interface vlan add name eth4-vlan2XX vlan-id=2XX interface=eth4   

/interface bridge add name=br-vlan1XX
/interface bridge add name=br-vlan2XX


/interface bridge port add interface=eth4-vlan1XX bridge=br-vlan1XX
/interface bridge port add interface=eth3 bridge=br-vlan1XX
/interface bridge port add interface=wFree  bridge=br-vlan1XX      


/interface bridge port add interface=eth4-vlan2XX bridge=br-vlan2XX
/interface bridge port add interface=wPrivate   bridge=br-vlan2XX

/interface print


'''
### [ejercicio3] Comenta cada una de estas líneas de la configuración
anterior para que quede bien documentado

-Linia1-Añadimos una vlan con el nombre vlan1XX a la interfaz eth4
-Linia2-Añadimos una vlan con el nombre vlan2XX a la interfaz eth4

-------

-Linia3--Linia4- Añadimos dos bridges con sus nombres respectivos

-------

-Linia5--Linia6-
Configuramos los puertos de los bridges ens las interfaces que correspondan
con la intencion de dirigir la salida

Finalmente mostramos la interfaz con todo:

 #     NAME                                TYPE       ACTUAL-MTU L2MTU  MAX-L2MTU MAC-ADDRESS      
 0     eth1                                ether            1500  1598       2028 6C:3B:6B:C2:27:93
 1  R  eth2                                ether            1500  1598       2028 6C:3B:6B:C2:27:94
 2   S eth3                                ether            1500  1598       2028 6C:3B:6B:C2:27:95
 3     eth4                                ether            1500  1598       2028 6C:3B:6B:C2:27:96
 4  XS wFree                               wlan             1500  1600       2290 6C:3B:6B:C2:27:97
 5  X  wPrivate                            wlan                   1600       2290 6E:3B:6B:C2:27:97
 6  R  br-vlan123                          bridge           1500  1594            6C:3B:6B:C2:27:96
 7  R  br-vlan223                          bridge           1500  1594            6C:3B:6B:C2:27:96
 8   S eth4-vlan123                        vlan             1500  1594            6C:3B:6B:C2:27:96
 9   S eth4-vlan223                        vlan             1500  1594            6C:3B:6B:C2:27:96


### [ejercicio4] Pon una ip 172.17.1XX.1/24 a br-vlan1XX 
y 172.17.2XX.1/24 a br-vlan2XX y verifica desde la interface de la placa 
base de tu ordenador que hay conectividad (puedes hacer ping) configurando
unas ips cableadas. Cuando lo hayas conseguido haz un backup de la configuración


ip address add address=172.17.123.1/24 interface=br-vlan123
ip address add address=172.17.223.1/24 interface=br-vlan223

/ip address print

 #   ADDRESS            NETWORK         INTERFACE                                                                                                            
 0   ;;; defconf
     192.168.88.1/24    192.168.88.0    eth2                                                                                                                 
 1   172.17.123.1/24    172.17.123.0    br-vlan123                                                                                                           
 2   172.17.223.1/24    172.17.223.0    br-vlan223 


Añadimos ips de la familia 172.17.1XX.X/24 y 172.17.2XX.X/24 a nuestra
targeta de red para testear los pings.
Solo funcionara el ping a la .123 ya que es la que tenemos conectada
en el puerto 2 del mikrotik.

[root@j23 ~]# ping 172.17.123.1
PING 172.17.123.1 (172.17.123.1) 56(84) bytes of data.
64 bytes from 172.17.123.1: icmp_seq=1 ttl=64 time=0.476 ms
64 bytes from 172.17.123.1: icmp_seq=2 ttl=64 time=0.244 ms
64 bytes from 172.17.123.1: icmp_seq=3 ttl=64 time=0.252 ms
64 bytes from 172.17.123.1: icmp_seq=4 ttl=64 time=0.253 ms

[root@j23 ~]# ping 172.17.223.1
PING 172.17.223.1 (172.17.223.1) 56(84) bytes of data.
From 172.17.223.90 icmp_seq=1 Destination Host Unreachable
From 172.17.223.90 icmp_seq=2 Destination Host Unreachable
From 172.17.223.90 icmp_seq=3 Destination Host Unreachable


Para que pueda hacer ping a las direcciones tenemos que crear dos vlans:

 ip link add link enp2s0 name v123 type vlan id 123
 ip link add link enp2s0 name v223 type vlan id 223

Luego les ponemos las ips de la familia y subimos las interfaces para que
todo funcione:

	ip a a 172.17.123.80/24 dev v123
	ip a a 172.17.223.80/24 dev v223

	ip link set v123 up
	ip link set v223 up

    ping 172.17.123.1
    ping 172.17.223.1

[root@j23 ~]# ping 172.17.123.1
PING 172.17.123.1 (172.17.123.1) 56(84) bytes of data.
64 bytes from 172.17.123.1: icmp_seq=1 ttl=64 time=0.296 ms
64 bytes from 172.17.123.1: icmp_seq=2 ttl=64 time=0.261 ms
64 bytes from 172.17.123.1: icmp_seq=3 ttl=64 time=0.284 ms
64 bytes from 172.17.123.1: icmp_seq=4 ttl=64 time=0.279 ms



[root@j23 ~]# ping 172.17.223.1
PING 172.17.223.1 (172.17.223.1) 56(84) bytes of data.
64 bytes from 172.17.223.1: icmp_seq=1 ttl=64 time=0.258 ms
64 bytes from 172.17.223.1: icmp_seq=2 ttl=64 time=0.264 ms
64 bytes from 172.17.223.1: icmp_seq=3 ttl=64 time=0.253 ms
64 bytes from 172.17.223.1: icmp_seq=4 ttl=64 time=0.280 ms
64 bytes from 172.17.223.1: icmp_seq=5 ttl=64 time=0.263 ms



### [ejercicio5] Crea una servidor dhcp para cada una de las redes en los rangos
.101 a .250 de las respectivas redes. Asignar ip manual a la eth1. 
Crear reglas de nat para salir a internet. 

Añadimos los rangos de ip's:

/ip pool add ranges=172.17.123.100-172.17.123.200 name=range_public
/ip pool add ranges=172.17.223.100-172.17.223.200 name=range_private


[admin@mkt01eric] > /ip pool print  
 # NAME                                                 RANGES                         
 0 range_public                               172.17.123.100-172.17.123.200  
 1 range_private                              172.17.223.100-172.17.223.200 


Damos los rangos de ip a cada interfaz:

 /ip dhcp-server add address-pool=range_public interface br-vlan123

/ip dhcp-server  add address-pool=range_private interface br-vlan223

/interface wireless set ssid=free123
numbers=0
/interface wireless set ssid=private223
numbers=1


[admin@mkt01eric] > /interface wireless print 
Flags: X - disabled, R - running 
 0    name="wFree" mtu=1500 l2mtu=1600 mac-address=6C:3B:6B:C2:27:97 arp=enabled interface-type=Atheros AR9300 mode=ap-bridge ssid="free123" frequency=auto 
      band=2ghz-b/g/n channel-width=20/40mhz-Ce scan-list=default wireless-protocol=802.11 vlan-mode=no-tag vlan-id=1 wds-mode=disabled 
      wds-default-bridge=none wds-ignore-ssid=no bridge-mode=enabled default-authentication=yes default-forwarding=yes default-ap-tx-limit=0 
      default-client-tx-limit=0 hide-ssid=no security-profile=default compression=no 

 1    name="wPrivate" mtu=1500 l2mtu=1600 mac-address=6E:3B:6B:C2:27:97 arp=enabled interface-type=virtual-AP master-interface=wFree ssid="private223" 
      vlan-mode=no-tag vlan-id=1 wds-mode=disabled wds-default-bridge=none wds-ignore-ssid=no bridge-mode=enabled default-authentication=yes 
      default-forwarding=yes default-ap-tx-limit=0 default-client-tx-limit=0 hide-ssid=no security-profile=default 



Añadimos la red:

/ip dhcp-server network add dns-server=8.8.8.8 gateway=172.17.123.1 netmask=24 domain=eric.com address=172.17.123.0/24 
/ip dhcp-server network add dns-server=8.8.8.8 gateway=172.17.223.1 netmask=24 domain=eric.com address=172.17.223.0/24

[admin@mkt01eric] > /ip dhcp-server network print 
 # ADDRESS            GATEWAY         DNS-SERVER      WINS-SERVER     DOMAIN                                                                                 
 0 172.17.123.0/24    172.17.123.1    8.8.8.8                         eric.com                                                                               
 1 172.17.223.0/24    172.17.223.1    8.8.8.8                         eric.com 


Activamos el dhcp-server:

/ip dhcp-server set 0 disabled=no

[admin@mkt01eric] > /ip dhcp-server print 
Flags: X - disabled, I - invalid 
 #   NAME                               INTERFACE                               RELAY           ADDRESS-POOL                               LEASE-TIME ADD-ARP
 0   dhcp1                              br-vlan123                                              range_public                               10m       
 1 X dhcp2                              br-vlan223                                              range_private                              10m 



Configuararem la ip per la interficie eth1 que va cap a internet. 
Definim la default gateway perque tingui ruta de sortida i creem les regles de routing.

/ip addres add addres=192.168.3.217/16 interface=eth1
/ip firewall nat action=masquerade chain=srcnat out-interface=eth1
/ip route add gateway=192.168.0.0 dst-addres=0.0.0.0 /0


Finalment observem que podem fer ping a internet des del mikrotik


[admin@mkt01eric] > ping 8.8.8.8
  SEQ HOST                                     SIZE TTL TIME  STATUS                                                                                         
    0 8.8.8.8                                    56  56 11ms 
    1 8.8.8.8                                    56  56 11ms 
    2 8.8.8.8                                    56  56 10ms 
    3 8.8.8.8                                    56  56 11ms 
    sent=4 received=4 packet-loss=0% min-rtt=10ms avg-rtt=10ms max-rtt=11ms 


	
### [ejercicio6] Activar redes wifi y dar seguridad wpa2

Per activar les reds wifi utilizarem el seguent command:

/interface wireless set 0,1 disabled=no

Per comprovar que ha funcionat podem mirar amb el mobil una red amb el nom
que te la ssid de la interficie:

[admin@mkt01eric] > /interface wireless print            
Flags: X - disabled, R - running 
 0   name="wFree" mtu=1500 l2mtu=1600 mac-address=6C:3B:6B:C2:27:97 arp=enabled interface-type=Atheros AR9300 mode=ap-bridge ssid="free123" frequency=auto 
      band=2ghz-b/g/n channel-width=20/40mhz-Ce scan-list=default wireless-protocol=802.11 vlan-mode=no-tag vlan-id=1 wds-mode=disabled 
      wds-default-bridge=none wds-ignore-ssid=no bridge-mode=enabled default-authentication=yes default-forwarding=yes default-ap-tx-limit=0 
      default-client-tx-limit=0 hide-ssid=no security-profile=default compression=no 

 1  R name="wPrivate" mtu=1500 l2mtu=1600 mac-address=6E:3B:6B:C2:27:97 arp=enabled interface-type=virtual-AP master-interface=wFree ssid="private223" 
      vlan-mode=no-tag vlan-id=1 wds-mode=disabled wds-default-bridge=none wds-ignore-ssid=no bridge-mode=enabled default-authentication=yes 
      default-forwarding=yes default-ap-tx-limit=0 default-client-tx-limit=0 hide-ssid=no security-profile=default 


Creamos un nuevo perfil al que cambiaremos la contraseña  y asignaremos a la red privada

/interface wireless security-profiles add name=profileprivate


Ahora añadimos el nuevo perfil creado al perfil de la red privada


/interface wireless set 1 security-profile=profileprivate 


Configuramos la contraseña de la red y escojemos el tipo de seguridad:

/interface wireless security-profiles edit 1 wpa2-pre-shared-key  


/interface wireless security-profiles  set 1 authentification-types-wpa2-psk


/interface wireless security-profiles set 1 mode=dynamic-keys



-- No se puede tener activas las dos redes ya que no es posible. Solamente puede
estar en funcionamente una de las dos. Para escojer cual quieres que funcione solo
le debemos dar ip a la interficie virtual con el dhclient i activar i desactivar las 
interficies respectivamente


### [ejercicio6b] Opcional (montar portal cautivo)



### [ejercicio7] Firewall para evitar comunicaciones entre las redes Free y Private. Limitar
puertos en Free.

Abrimos el winbox y entramos dentro del mikrotik. Nos vamos a la opcion de firewall y...

Buscamos dentro de las normas de filto la de "accept ICMP".
Entramos en su configuracion y hacmos drop. Luego  configuramos su cadena y la ponemos en firewall
y agreamos su ip origen y destino.

Ip origen:172.17.123.0/24
Ip destino:172.17.223.0/24


[isx47983457@j23 ~]$ ping 172.17.223.197
PING 172.17.223.197 (172.17.223.197) 56(84) bytes of data.
^C
--- 172.17.223.197 ping statistics ---
10 packets transmitted, 0 received, 100% packet loss, time 8999ms

[isx47983457@j23 ~]$ 


### [ejercicio7b] Seguridad en acceso a router mikrotik

Cambiamos el puerto donde escucha el el ssh:

/ip service
set ssh port=22340

Permitimos que desde cualquier ip se puedan conectar a los puertos del router que queramos:


/ip firewall filter
add chain=input comment="defconf: accept ssh on port 22340" \
    dst-port=22340 protocol=tcp 


Si quiero añadir esa regla la primera de todas si ya disponemos de otras reglas usamos place-before:

/ip firewall filter
add chain=input comment="defconf: accept ssh on port 22340" \
    dst-port=22340 protocol=tcp place-before=0

Cambiamos el password (si no tiene no hace falta poner old-password):

/password new-password=clase old-password=clase confirm-new-password=clase

Sincronizamos el reloj del router usando el protocolo ntp a partir de alguna ip de los servidores de tiempo. En un linux:

/system ntp client
set enabled=yes primary-ntp=213.251.52.234 secondary-ntp=158.227.98.15



### [ejercicio8] Conexión a switch cisco con vlans

### [ejercicio8b] (Opcional) Montar punto de acceso adicional con un AP de otro fabricante

### [ejercicio9] QoS

### [ejercicio9] Balanceo de salida a internet

### [ejercicio10] Red de servidores con vlan de servidores y redirección de puertos

### [ejercicio11] Firewall avanzado

