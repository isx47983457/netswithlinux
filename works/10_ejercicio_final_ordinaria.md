Una organización está organizada en dos departamentos:
- deptA
- deptB

Cada departamento ha de estar aislado del otro y ha de poder acceder
a una red de servidores que llamaremos "servers". También se dipone de 
una red wifi que no ha de poder acceder a las redes internas, y que sólo debe dejar
acceder al puerto 80 y 443 del servidor web de la red de servers. Desde
la red wifi también se podrá navegar a internet usando los puertos destino 80 y 443, pero
no se podrán establecer conexiones telnet, ssh o ftp.

Se ha contratado una línea empresarial a un ISP, que ha de ser utilizada
sólo por los servidores, excepto en caso de caída del "router inf" que
es el que se usa habitulamente como router de salida de las redes de 
los departamentos y de los dispositivos conectados por wifi.

Se ha de priorizar el tráfico del deptA respecto al deptB y se ha de 
repartir el ancho de banda por igual entre los usuarios de cada departamento.

Se ha de limitar el tráfico de bajada de la wifi a 500Kbps

El servidor ha de ser accesible desde inernet usando el puerto externo 77XX

Se ha de limitar el acceso por ssh (cambiando el puerto al 12322) al 
router desde cualquier red, y sólo se ha de poder hacer telnet desde la red de control

Se disone de un mikrotik de 4 puertos y de 6 puertos del switch para cada
puesto de trabajo que se conectarán de la siguiente forma:


MIKROTIK                   tagged               untagged   destino 
--------------------------------------------------------------------------
puerto 1   internet        escola,isp                      sw-puerto4 
puerto 2   puerto usb pc                                   pc-usb-ethernet 
puerto 3   redes internas  deptA,deptB,servers             sw-puerto5 
puerto 4      

     
SWITCH                   tagged          untagged type destino
-------------------------------------------------------------------------------------------
puerto 1   deptA                                         deptA      access    PC
puerto 2   detpB                                         deptB      access    PC
puerto 3   servers                                       servers    access    PC
puerto 4   mkt-internet         escola,isp                          trunk     mkt-puerto1
puerto 5   mkt-xarxes_internes  deptA,deptB,servers                 trunk     mkt-puerto3
puerto 6   roseta               deptA,deptB,servers,isp  escola     hybrid    roseta


Tabla de redes, ips y vlans:

REDES      VLAN      NETWORK-ADDR    IP-ADDR-MKT   DHCP-RANGE
---------------------------------------------------------------
deptA      7xx      10.27.1XX.0/24   .1            .100 - .200
deptB      8xx      10.28.1XX.0/24   .1            .100 - .200
servers    9xx      10.29.1XX.0/24   .1            NO DHCP
escola     1000     192.168.0.0/16   .3.2XX        NO DHCP
isp        1005       10.30.0.0/24   .2XX          NO DHCP
wireless   NO-VLAN  10.50.1XX.0/24   .1            .10 - .250
gestion    NO-VLAN 192.168.88.0/24   .1            NO DHCP


El esquema a nivel de capa 3 es el siguiente.


                    +-----------+             XX XXXXX XXXX
                    |router inf +----------+XXXXXX         XX
                    +-----------+        XX                 X
                          |.1            XX   internet      X  +-------------+
                          |               X              XXXX  |  router ISP |
             -------------+----+-+        XXXXXXXXXXXXXXX+-----+             |
               192.168.0.0/16  |                               +-------------+
                               |                                    |.1
                               |             10.30.0.0/24           |
                               |    +-+-----------------------------++
  +------+                     |      |
  |      |              escola |      | isp
  |server|            vlan1000 |      | vlan1005
  |web   |               port1 |      | port1
  +------+               .3.2XX|      |.2xx
     |.100                  +-------------+
     |                      |             |       +))
     |    10.29.1xx.0/24  .1|             |.1     |wireless
+----+----------------------+   MIKROTIK  +-------+
            vlan9xx    port3|             |    10.50.1xx.0/24
            servers         |             |
                            +--+-----+----+
                            .1 |     |  .1
                       vlan7xx |     |  vlan8xx
                         port3 |     |  port3
                         deptA |     |  deptB
                               |     |
                               |     |
      +------------------------+-+   |
          10.27.1xx.0/24           +-+------------------------------+
                                         10.28.1xx.0/24


# Resolución

Pasos:
1. Configurar switch

!PUERTO DE ACCESO PARA dept A
interface fa0/7
switchport mode access
switchport access vlan 728

!PUERTO DE ACCESO PARA dept B
interface fa0/8
switchport mode access
switchport access vlan 828

!PUERTO DE ACCESO PARA servers
interface fa0/9
switchport mode access
switchport access vlan 928

!PUERTO DE TRUNK PARA mkt salida internet
interface fa0/10
switchport mode trunk
switchport trunk allowed vlan 1000,1005

!PUERTO DE TRUNK PARA mkt redes internas
interface fa0/11
switchport mode trunk
switchport trunk allowed vlan 728,828,928

!PUERTO HYBRIDO a la roseta
interface fa0/12
switchport mode access
switchport access vlan 1000




2. Configurar interfaces virtuales y direcciones IP en el router


/interface vlan add vlan-id=728 interface=eth3 name=deptA
/interface vlan add vlan-id=828 interface=eth3 name=deptB
/interface vlan add vlan-id=928 interface=eth3 name=servers
/interface vlan add vlan-id=1000 interface=eth1 name=escola
/interface vlan add vlan-id=1005 interface=eth1 name=isp

/ip address add interface=deptA address=10.27.128.1/24
/ip address add interface=deptB address=10.28.128.1/24
/ip address add interface=servers address=10.29.128.1/24
/ip address add interface=escola address=192.168.3.228/16
/ip address add interface=isp address=10.30.0.128/24


 ip address print 
Flags: X - disabled, I - invalid, D - dynamic 
 #   ADDRESS            NETWORK         INTERFACE                                
 0   ;;; defconf
     192.168.88.1/24    192.168.88.0    eth2                                     
 1   10.27.112.1/24     10.27.112.0     deptA                                    
 2   10.28.112.1/24     10.28.112.0     deptB                                    
 3   10.29.112.1/24     10.29.112.0     server                                   
 4   192.168.3.212/16   192.168.0.0     escola                                   
 5   10.30.0.112/24     10.30.0.0       isp
 



3. Configurar servidores dhcp

/ip pool
add name=deptA ranges=10.27.128.100-10.27.128.200
add name=deptB ranges=10.28.128.100-10.28.128.200
add name=wireless ranges=10.50.128.10-10.50.128.250



/ip dhcp-server network
add address=10.27.129.0/24 dns-server=8.8.8.8 domain=eric.com gateway=10.27.129.1 netmask=24
add address=10.28.129.0/24 dns-server=8.8.8.8 domain=eric.com gateway=10.28.129.1 netmask=24
add address=10.50.129.0/24 dns-server=8.8.8.8 domain=eric.com gateway=10.50.129.1 netmask=24


/ip	dhcp-server
add address-pool=range-deptA  interface=deptA  
add address-pool=range-deptB  interface=deptB
ip dhcp-server add address-pool=range-wifi interface=wlan1  


/ip firewall nat
add action=masquerade chain=srcnat out-interface=escola
add action=masquerade chain=srcnat out-interface=isp




4. Definir default gateway por router inf y verificar que salen, cambiar
default gateway por router-isp y verificar que salen. Dejar la ruta con más 
peso a router-inf. Verificar que al desconectar router-inf se cambia la salida por
router-isp

/ip route add gateway=192.168.0.1 dst-addres=0.0.0.0/0  
/ip route add  dst-address=0.0.0.0/0 gateway=10.30.0.1 distance=2

ip route print                                       
Flags: X - disabled, A - active, D - dynamic, 
C - connect, S - static, r - rip, b - bgp, o - ospf, m - mme, 
B - blackhole, U - unreachable, P - prohibit 
 #      DST-ADDRESS        PREF-SRC        GATEWAY            DISTANCE
 0 A S  0.0.0.0/0                          192.168.0.1               1
 1   S  0.0.0.0/0                          10.30.0.1                 2
 2 ADC  10.27.112.0/24     10.27.112.1     deptA                     0
 3 ADC  10.28.112.0/24     10.28.112.1     deptB                     0
 4 ADC  10.29.112.0/24     10.29.112.1     server                    0
 5 ADC  10.30.0.0/24       10.30.0.112     isp                       0
 6 ADC  10.50.112.0/24     10.50.112.1     wlan1                     0
 7 ADC  192.168.0.0/16     192.168.3.212   escola                    0
 8 ADC  192.168.88.0/24    192.168.88.1    eth2                      0



[admin@mkt01eric] > ip route print 
Flags: X - disabled, A - active, D - dynamic, C - connect, S - static, r - rip, b - bgp, o - ospf, m - mme, B - blackhole, U - unreachable, P - prohibit 
 #      DST-ADDRESS        PREF-SRC        GATEWAY            DISTANCE
 0 A S  0.0.0.0/0                          192.168.0.1               1
 1 ADC  10.27.128.0/24     10.27.128.1     deptA                     0
 2 ADC  10.28.128.0/24     10.28.128.1     deptB                     0
 3 ADC  10.29.128.0/24     10.29.128.1     servers                   0
 4 ADC  10.30.0.0/24       10.30.0.128     isp                       0
 5 ADC  192.168.0.0/16     192.168.3.228   escola                    0
 6 ADC  192.168.88.0/24    192.168.88.1    eth2                      0




5. Activar las marcas de rutas para que desde la vlan de servers se salga
por router-isp y desde el resto de redes se salga por router-inf


ip route add dst-address=0.0.0.0/0 gateway=10.30.0.1 distance=10

ip firewall mangle add action=mark-connection chain=prerouting  connection-state=new new-connection-mark=from_servers src-address=10.29.128.0/24

ip firewall mangle add action=mark-routing chain=prerouting in-interface=server new-routing-mark=server_to_isp
 
ip route add dst-address=0.0.0.0/0 gateway=10.30.0.1 routing-mark=from_servers 


ip route print 
Flags: X - disabled, A - active, D - dynamic, 
C - connect, S - static, r - rip, b - bgp, o - ospf, m - mme, 
B - blackhole, U - unreachable, P - prohibit 
 #      DST-ADDRESS        PREF-SRC        GATEWAY            DISTANCE
 0 A S  0.0.0.0/0                          10.30.0.1                 1
 1 A S  0.0.0.0/0                          192.168.0.1               1
 2   S  0.0.0.0/0                          10.30.0.1                 2
 3 ADC  10.27.112.0/24     10.27.112.1     deptA                     0
 4 ADC  10.28.112.0/24     10.28.112.1     deptB                     0
 5 ADC  10.29.112.0/24     10.29.112.100   server                    0
 6 ADC  10.30.0.0/24       10.30.0.112     isp                       0
 7  DC  10.50.112.0/24     10.50.112.1     wlan1                   255
 8 ADC  192.168.0.0/16     192.168.3.212   escola                    0
 9 ADC  192.168.88.0/24    192.168.88.1    eth2                      0




6. Reglas de firewall para evitar accesos desde deptA a deptB y desde
wifi solo a server-web y a internet


-- lea solo una vez cada conection

ip firewall filter add chain=forward connection-state=established,
related  action=accept place-before=0

# Entre deptA y deptB no habra conexion

/ip firewall filter 
add action=reject chain=forward comment="De deptA a deptB drop" in-interface=deptA out-interface=deptB
add action=reject chain=forward comment="de deptB a deptA drop" in-interface=deptB out-interface=deptA

# Bloquea el trafico y lo filtra solo para que se conecte desde el wifi a server-web y internet haciendo drop de lo demas

ip firewall filter add  action=accept  chain=forward in-interface=wlan1 dst-address=10.29.129.100/24   
ip firewall filter add chain=forward in-interface=wlan1 out-interface=escola action=accept 
ip firewall filter add chain=forward in-interface=wlan1 out-interface=isp  action=accept     
ip firewall filter add chain=forward in-interface=wlan1 action=drop 





7. Reglas de firewall para que sólo haya telnet desde ips de gestión 
y servidor ssh por puerto 12322 	

 TELNET 

ip firewall filter add action=drop dst-port=23 protocol=tcp chain=input  
ip firewall filter add action=accept  chain=input dst-address=192.168.88.0/24  protocol=tcp  


SSH

ip service set  ssh port=12322



 ip firewall filter print
Flags: X - disabled, I - invalid, D - dynamic 
 0  D ;;; special dummy rule to show fasttrack counters
      chain=forward 

 1    chain=forward action=accept connection-state=established,related log=no 
      log-prefix="" 

 2    ;;; defconf: accept ICMP
      chain=input action=accept protocol=icmp log=no log-prefix="" 

 3    ;;; defconf: accept establieshed,related
      chain=input action=accept connection-state=established,related log=no 
      log-prefix="" 

 4    ;;; defconf: drop all from WAN
      chain=input action=drop in-interface=eth1 log=no log-prefix="" 

 5    ;;; defconf: fasttrack
      chain=forward action=fasttrack-connection 
      connection-state=established,related log=no log-prefix="" 

 6    ;;; defconf: accept established,related
      chain=forward action=accept connection-state=established,related log=no 
      log-prefix="" 

 7    ;;; defconf: drop invalid
      chain=forward action=drop connection-state=invalid log=no log-prefix="" 

 8    ;;; defconf:  drop all from WAN not DSTNATed
      chain=forward action=drop connection-state=new 
      connection-nat-state=!dstnat in-interface=eth1 log=no log-prefix="" 

 9    chain=forward action=drop src-address=10.27.112.0/24 
      dst-address=10.28.112.0/24 log=no log-prefix="" 

10    chain=forward action=drop src-address=10.28.112.0/24 
      dst-address=10.27.112.0/24 log=no log-prefix="" 

11    chain=forward action=accept dst-address=10.29.112.100 in-interface=wlan1 
      log=no log-prefix="" 

12    chain=forward action=accept in-interface=wlan1 out-interface=escola log=no 
      log-prefix="" 

13    chain=forward action=accept in-interface=wlan1 out-interface=isp log=no 
      log-prefix="" 

14    chain=forward action=drop in-interface=wlan1 log=no log-prefix="" 

15    chain=input action=accept protocol=tcp src-address=192.168.88.0/24 
      dst-port=23 log=no log-prefix="" 

16    chain=input action=drop protocol=tcp dst-port=23 log=no log-prefix="" 




8. Limitar ancho de banda de bajada en wireless a 500Kbps

queue tree add limit-at=500k max-limit=500k packet-mark=no-mark parent=wlan1


10. Repartir por igual el tráfico entre todos los usuarios de deptA y deptB



 1   name="deptB" parent=deptB packet-mark=no-mark limit-at=0 
     queue=pcq-download-default priority=8 max-limit=0 burst-limit=0 
     burst-threshold=0 burst-time=0s 

 2   name="deptA" parent=deptA packet-mark=no-mark limit-at=0 
     queue=pcq-download-default priority=8 max-limit=0 burst-limit=0 
     burst-threshold=0 burst-time=0s 



