isx47983457
Eric Escriba
escoladeltreball

EJERCICIO MIKROTIK 1.

OBJETIVO: FAMILIARIZARSE CON LAS OPERACIONES BÁSICAS EN UN ROUTER MIKROTIK

#1. Entrar al router con telnet, ssh y winbox
telent -> telnet nombre_del_servidor  telnet 192.168.88.1 
ssh -> ssh usuario@servidor ssh admin@192.168.88.1
winbox -> 

#2. Como resetear el router

Vamos a system y luego a reset configuration. Finalmente aceptamos que
se resetee.

#3. Cambiar nombre del dispositivo, password

Vamos a system -> identity -> set name="nombre"
Escribimos password y podemos cambiar la password

#4. Esquema de configuración de puertos inicial

[admin@infMKT01-eric] /interface> print 
Flags: D - dynamic, X - disabled, R - running, S - slave 
 #     NAME                                TYPE       ACTUAL-MTU L2MTU  MAX-L2MTU MAC-ADDRESS      
 0     ether1                              ether            1500  1598       2028 6C:3B:6B:C2:27:93
 1  RS ether2-master                       ether            1500  1598       2028 6C:3B:6B:C2:27:94
 2   S ether3                              ether            1500  1598       2028 6C:3B:6B:C2:27:95
 3   S ether4                              ether            1500  1598       2028 6C:3B:6B:C2:27:96
 4   S wlan1                               wlan             1500  1600       2290 6C:3B:6B:C2:27:97
 5  R  ;;; defconf
       bridge                              bridge           1500  1598            6C:3B:6B:C2:27:94
	

#5. Cambiar nombres de puertos y deshacer configuraciones iniciales

Vamos a interface y escribimos /interface> set ether1 name="-----"


#6. Backup y restauración de configuraciones

Vamos a system luego a backup y escojemos la opción save para
guardar la configuración actual.

system backup save

En el mismo directorio escojemos la opción load para restaurar la
configuración.

system backup load
